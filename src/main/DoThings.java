package main;

import model.CanBo;

import java.util.ArrayList;
import java.util.stream.Stream;

public class DoThings {
    //Tim kiem can bo theo ten
    static void find(String name, ArrayList<CanBo> list) {
        int count = (int) list.stream().filter(canBo -> canBo.getHoTen().contains(name)).count();
        if (count == 0) {
            System.out.println("Khong co can bo can tim kiem...");
        } else {
            Stream<CanBo> stream = list.stream().filter(canBo -> canBo.getHoTen().contains(name));
            stream.forEach(System.out::println);
        }
    }
    //Them moi can bo
    static <T> void add(T t, ArrayList<T> list) {
        list.add(t);
        System.out.println("Da them thanh cong...");
    }
}
