package main;

import exception.AgeException;
import exception.GenderExeption;
import exception.HandleException;
import exception.RankException;
import interfaces.AddNewInterface;
import interfaces.FindInterface;
import model.CanBo;
import model.CongNhan;
import model.KySu;
import model.NhanVien;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;

public class QLCB {
    static ArrayList<CanBo> listCB = new ArrayList<>();
    static ArrayList<CongNhan> listCN = new ArrayList<>();
    static ArrayList<KySu> listKS = new ArrayList<>();
    static ArrayList<NhanVien> listNV = new ArrayList<>();

    //Danh sach chuc nang
    public static void function() {
        System.out.println("---------Function---------");
        System.out.println("Moi chon (1/2/3/4):");
        System.out.println("1. Them moi can bo.");
        System.out.println("2. Tim kiem can bo.");
        System.out.println("3. Xem danh sach can bo.");
        System.out.println("4. Dong ung dung");
        System.out.println("--------------------------");

        Scanner scanner = new Scanner(System.in);
        int chon = Integer.parseInt(scanner.nextLine());
        switch (chon) {
            case 1:
                addNew();
                break;
            case 2:
                find();
                break;
            case 3:
                view();
                break;
            case 4:
                System.out.println("Ban da thoat ung dung.");
                System.exit(0);
                break;
            default:
                System.out.println("Vui long chon tu 1 den 4");
                System.out.println("----------");
                function();
                break;
        }
    }

    //Danh sach them moi
    static void addNew() {
        System.out.println("---Them moi can bo---");
        System.out.println("1. Them moi Cong nhan.");
        System.out.println("2. Them moi Ky su.");
        System.out.println("3. Them moi Nhan vien.");
        System.out.println("4. Quay lai.");
        System.out.println("Moi chon (1/2/3/4)..");
        System.out.println("----------");

        Scanner scanner = new Scanner(System.in);
        int chon = Integer.parseInt(scanner.nextLine());
        switch (chon) {
            case 1:
                addNewCN();
                break;
            case 2:
                addNewKS();
                break;
            case 3:
                addNewNV();
                break;
            case 4:
                function();
                break;
            default:
                addNew();
                break;
        }
    }

    //Them moi cong nhan
    static void addNewCN() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhap thong tin cong nhan:");
        System.out.println("1. Ho va ten");
        String name = scanner.nextLine();
        System.out.println("2. Gioi tinh (Nam/Nu/Khac):");
        String gender = scanner.nextLine();

        try {
            HandleException.inputGender(gender);
        } catch (GenderExeption genderExeption) {
            System.out.println(genderExeption);
            System.out.println("----------");
            addNewCN();
        }

        System.out.println("3. Tuoi:");
        String age1 = scanner.nextLine();

        try {
            int age = Integer.parseInt(age1);
            HandleException.rangeOfAge(age);
        } catch (AgeException ageException) {
            System.out.println(ageException);
            System.out.println("----------");
            addNewCN();
        } catch (NumberFormatException exception) {
            System.out.println("Vui long nhap chu so");
            System.out.println("----------");
            addNewCN();
        }
        int age = Integer.parseInt(age1);

        System.out.println("4. Bac:");
        String rank1 = scanner.nextLine();

        try {
            int rank = Integer.parseInt(rank1);
            HandleException.inputRank(rank);
        } catch (RankException e) {
            System.out.println(e);
            System.out.println("----------");
            addNewCN();
        } catch (NumberFormatException exception) {
            System.out.println("Vui long nhap chu so");
            System.out.println("----------");
            addNewCN();
        }

        int rank = Integer.parseInt(rank1);
        CongNhan cn = new CongNhan(name, gender, age, rank);

        AddNewInterface addNewInterface = DoThings::add;
        addNewInterface.add(cn, listCN);
        System.out.println("----------");

        addNew();

    }

    //Them moi ky su
    static void addNewKS() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhap thong tin ky su:");
        System.out.println("1. Ho va ten");
        String name = scanner.nextLine();
        System.out.println("2. Gioi tinh (Nam/Nu/Khac):");
        String gender = scanner.nextLine();

        try {
            HandleException.inputGender(gender);
        } catch (GenderExeption genderExeption) {
            System.out.println(genderExeption);
            System.out.println("----------");
            addNewKS();
        }

        System.out.println("3. Tuoi:");
        String age1 = scanner.nextLine();

        try {
            int age = Integer.parseInt(age1);
            HandleException.rangeOfAge(age);
        } catch (AgeException ageException) {
            System.out.println(ageException);
            System.out.println("----------");
            addNewKS();
        } catch (NumberFormatException exception) {
            System.out.println("Vui long nhap chu so");
            System.out.println("----------");
            addNewKS();
        }
        int age = Integer.parseInt(age1);

        System.out.println("4. Nganh:");
        String major = scanner.nextLine();

        KySu ks = new KySu(name, gender, age, major);

        AddNewInterface addNewInterface = DoThings::add;
        addNewInterface.add(ks, listKS);
        System.out.println("----------");

        addNew();

    }

    //Them moi nhan vien
    static void addNewNV() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhap thong tin nhan vien:");
        System.out.println("1. Ho va ten");
        String name = scanner.nextLine();
        System.out.println("2. Gioi tinh (Nam/Nu/Khac):");
        String gender = scanner.nextLine();

        try {
            HandleException.inputGender(gender);
        } catch (GenderExeption genderExeption) {
            System.out.println(genderExeption);
            System.out.println("----------");
            addNewNV();
        }

        System.out.println("3. Tuoi:");
        String age1 = scanner.nextLine();

        try {
            int age = Integer.parseInt(age1);
            HandleException.rangeOfAge(age);
        } catch (AgeException ageException) {
            System.out.println(ageException);
            System.out.println("----------");
            addNewNV();
        } catch (NumberFormatException exception) {
            System.out.println("Vui long nhap chu so");
            System.out.println("----------");
            addNewNV();
        }
        int age = Integer.parseInt(age1);

        System.out.println("4. Cong viec:");
        String job = scanner.nextLine();

        NhanVien nv = new NhanVien(name, gender, age, job);

        AddNewInterface addNewInterface = DoThings::add;
        addNewInterface.add(nv, listNV);
        System.out.println("----------");

        addNew();

    }

    //Tim kiem can bo theo ten
    static void find() {
        if(listCB.size() > 0){
            listCB.clear();
        }
        listCB.addAll(listCN);
        listCB.addAll(listKS);
        listCB.addAll(listNV);

        System.out.println("Nhap ten can bo.");
        Scanner scanner = new Scanner(System.in);
        String ten = scanner.nextLine();
        FindInterface findInterface = DoThings::find;
        findInterface.findByName(ten, listCB);
        System.out.println("----------");

        function();
    }

    //Chuc nang xem danh sach
    static void view() {
        System.out.println("---Xem danh sach---");
        System.out.println("1. Danh sach tat ca can bo.");
        System.out.println("2. Danh sach cong nhan.");
        System.out.println("3. Danh sach ky su.");
        System.out.println("3. Danh sach nhan vien.");
        System.out.println("5. Quay lai.");
        System.out.println("Moi chon (1/2/3/4/5)..");
        System.out.println("----------");

        Scanner scanner = new Scanner(System.in);
        int chon = Integer.parseInt(scanner.nextLine());
        switch (chon) {
            case 1:
                viewAll();
                break;
            case 2:
                viewCN();
                break;
            case 3:
                viewKS();
                break;
            case 4:
                viewNV();
                break;
            case 5:
                function();
                break;
            default:
                view();
                break;
        }
    }

    //Xem danh sach tat ca can bo
    static void viewAll() {
        if(listCB.size() > 0){
            listCB.clear();
        }
        listCB.addAll(listCN);
        listCB.addAll(listKS);
        listCB.addAll(listNV);

        listCB.sort(Comparator.comparing(CanBo::toString));

        for (CanBo cb : listCB) {
            System.out.println(cb.toString());
        }
        System.out.println("----------");
        function();
    }

    //Xem danh sach cong nhan
    static void viewCN() {
        listCN.sort(Comparator.comparing(CongNhan::toString));

        for (CongNhan cn : listCN) {
            System.out.println(cn.toString());
        }
        System.out.println("----------");
        function();
    }

    //Xem danh sach ky su
    static void viewKS() {
        listKS.sort(Comparator.comparing(KySu::toString));

        for (KySu ks: listKS) {
            System.out.println(ks.toString());
        }
        System.out.println("----------");
        function();
    }

    //Xem danh sach nhan vien
    static void viewNV() {
        listNV.sort(Comparator.comparing(NhanVien::toString));

        for (NhanVien nv : listNV) {
            System.out.println(nv.toString());
        }
        System.out.println("----------");
        function();
    }

    public static void main(String[] args) {
        function();
    }
}
