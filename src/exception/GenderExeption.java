package exception;

//Xu ly ngoai le nhap gioi tinh
public class GenderExeption extends Exception{
    private String gender;

    public GenderExeption(String gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "GenderException:" + gender + "\n chi duoc dien Nam/Nu/Khac." ;
    }
}
