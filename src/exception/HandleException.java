package exception;

public class HandleException {
    public static void inputGender(String gender) throws GenderExeption {
        if(!(gender.equalsIgnoreCase("Nam")
                || gender.equalsIgnoreCase("Nu")
                || gender.equalsIgnoreCase("Khac"))) {
            throw new GenderExeption(gender);
        }
    }

    public static void inputRank(int rank) throws RankException {
        if(rank < 1 || rank > 10) {
            throw new RankException(rank);
        }
    }

    public static void rangeOfAge(int age) throws AgeException {
        if(age < 18 || age > 60) {
            throw new AgeException(age);
        }
    }
}
