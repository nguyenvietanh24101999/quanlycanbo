package exception;

//Xu ly ngoai le nhap Bac tu 1-10
public class RankException extends Exception{
    private int rank;

    public RankException(int rank) {
        this.rank = rank;
    }

    @Override
    public String toString() {
        return "RankException: " + rank
                + "\n vui long nhap tu 1 den 10";
    }
}
