package exception;

//Xu ly ngoai le nhap tuoi
public class AgeException extends Exception{
    private int age;

    public AgeException(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "AgeException: " + age +
                "\n Vui long nhap do tuoi tu 18 den 60";
    }
}
