package interfaces;

import model.CanBo;

import java.util.ArrayList;

@FunctionalInterface
public interface FindInterface {
    //Tim kiem can bo
    void findByName(String name, ArrayList<CanBo> list);
}
