package interfaces;

import model.CanBo;

import java.util.ArrayList;

@FunctionalInterface
public interface AddNewInterface<T> {
    //them can bo
    void add(T t, ArrayList<T> list);
}
