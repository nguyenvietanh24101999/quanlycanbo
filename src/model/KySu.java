package model;

public class KySu extends CanBo{
    private String nganh;

    public KySu(String hoTen, String gioiTinh, int tuoi, String nganh) {
        super(hoTen, gioiTinh, tuoi);
        this.nganh = nganh;
    }

    public String getNganh() {
        return nganh;
    }

    public void setNganh(String nganh) {
        this.nganh = nganh;
    }

    @Override
    public String toString() {
        return "Ky su: "
                + this.getHoTen() + ", gioi tinh: "
                + this.getGioiTinh() + ", tuoi: "
                + this.getTuoi() + ", nganh dao tao: "
                + this.getNganh();
    }
}
