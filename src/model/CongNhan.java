package model;

public class CongNhan extends CanBo {
    private int bac;

    public CongNhan(String hoTen, String gioiTinh, int tuoi) {
        super(hoTen, gioiTinh, tuoi);
    }

    public CongNhan(String hoTen, String gioiTinh, int tuoi, int bac) {
        super(hoTen, gioiTinh, tuoi);
        this.bac = bac;
    }

    public int getBac() {
        return bac;
    }

    public void setBac(int bac) {
        this.bac = bac;
    }

    @Override
    public String toString() {
        return "Cong nhan: "
                + this.getHoTen() + ", gioi tinh: "
                + this.getGioiTinh() + ", tuoi: "
                + this.getTuoi() + ", bac: "
                + this.getBac();
    }
}
